<?php

namespace App\Models;

class Monkey extends Animal
{
    public function calculateNewHealth(): void
    {
        if($this->health < 30){
            $this->health = 0;
        }
    }

    public function getName(): string
    {
        return 'monkey_'.$this->tag;
    }

    public function getIcon(): string
    {
        return '🐒';
    }
}