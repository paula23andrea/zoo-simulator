<?php

namespace App\Core;

use App\Services\RedisSerializer;
use DI\Container;
use DI\ContainerBuilder;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class BaseController
{
    public array $vars = [];
    public Container $container;

    public function __construct()
    {
        $builder = new ContainerBuilder();
        $builder->addDefinitions([
            'Templating' => new Environment((new FilesystemLoader(__DIR__.'/../Views'))),
            'RedisSerializer' => new RedisSerializer(Redis::getRedisClient()),
        ]);
        $this->container = $builder->build();
    }

    /**
     * @param string $template
     * @param array $variables
     */
    public function render(string $template, array $variables) {
        echo $this->get('Templating')->render($template, $variables);exit();
    }

    public function get($dependency){
        return $this->container->get($dependency);
    }

    public function redirect($route){
        header("Location: ".$route);
        die();
    }
}
