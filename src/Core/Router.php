<?php

namespace App\Core;

class Router
{
    public static function parse($url, $request)
    {
        $url = trim($url);
        $explode_url = explode('/', $url);
        if($url == '/') {
            $explode_url = array('Zoo', 'index');
        } else {
            array_shift($explode_url);
        }
        $request->controller = $explode_url[0];
        $request->action = $explode_url[1];

        $request->params = array_slice($explode_url, 2);
        $request->params = $request->params?$request->params[0]:null;
        $request->method = $_SERVER['REQUEST_METHOD'];
        if ($request->method === 'POST') {
            $request->params = $_POST;
        }
        $request->query = $_GET;
    }
}
