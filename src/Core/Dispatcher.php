<?php
namespace App\Core;

class Dispatcher
{
    private $request;
    public function dispatch()
    {
        $this->request = new Request();
        Router::parse($this->request->url, $this->request);
        $controller = $this->loadController();
        $action = $this->request->action;
        $controller->$action($this->request->params);
    }
    public function loadController()
    {
        $name = $this->request->controller . "Controller";
        $file = 'App\\Controllers\\' . $name;
        return new $file();
    }
}
