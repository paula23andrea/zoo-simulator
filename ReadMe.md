Navarik test - PABH
===================

_You must have installed_
* php >= 8.1, redis, composer

Installing steps
----------------
* `git clone https://gitlab.com/paula23andrea/zoo-simulator.git`
* `cd zoo-simulator`
* `composer install`
* `cp .env.dist .env` and add corresponding parameters

Run application 
----------------
* `php -S localhost:8888 -t Webroot`
* Open in browser `localhost:8888`

