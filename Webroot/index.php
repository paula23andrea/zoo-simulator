<?php

define('WEBROOT', str_replace("Webroot/index.php", "", $_SERVER["SCRIPT_NAME"]));
define('ROOT', str_replace("Webroot/index.php", "", $_SERVER["SCRIPT_FILENAME"]));
require_once ROOT . "/vendor/autoload.php";
$dispatch = new \App\Core\Dispatcher();
$dispatch->dispatch();
