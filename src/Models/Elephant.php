<?php

namespace App\Models;

class Elephant extends Animal
{
    private ?float $previousHealth;

    /**
     * @param int $tag
     * @param float|null $previousHealth
     */
    public function __construct(protected int $tag, ?float $previousHealth)
    {
        parent::__construct($this->tag);
        $this->previousHealth = $previousHealth;
    }

    public function calculateNewHealth(): void
    {
        if($this->health < 70 && $this->previousHealth < 70){
            $this->health = 0;
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'elephant_'.$this->tag;
    }

    /**
     * @param float $previousHealth
     */
    public function setPreviousHealth(float $previousHealth): void
    {
        $this->previousHealth = $previousHealth;
    }

    public function getIcon(): string
    {
        return $this->getHealth() < 70 ? '♿': '🐘';
    }
}