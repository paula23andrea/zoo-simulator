<?php
require_once __DIR__ . "/../vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$dotenv = Dotenv\Dotenv::create(__DIR__.'/../');
$dotenv->load();
Predis\Autoloader::register();

