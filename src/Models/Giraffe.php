<?php

namespace App\Models;

class Giraffe extends Animal
{
    public function calculateNewHealth(): void
    {
        if($this->health < 50){
            $this->health = 0;
        }
    }

    public function getName(): string
    {
        return 'giraffe_'.$this->tag;
    }

    public function getIcon(): string
    {
        return '🦒';
    }
}