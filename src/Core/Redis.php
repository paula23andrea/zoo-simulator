<?php

namespace App\Core;

require_once __DIR__ . "/../../vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager as BaseManager;
use Dotenv\Dotenv;
use Predis\Client;

class Redis
{
    public static function getRedisClient(): Client
    {
        $dotenv = Dotenv::create(__DIR__.'/../../');
        $dotenv->load();

        $cacheUrl = getenv('REDIS_URL','tcp://10.0.0.1:6379');

        return new Client($cacheUrl);
    }
}