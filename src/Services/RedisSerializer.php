<?php


namespace App\Services;

use App\Models\Animal;
use App\Models\Elephant;
use App\Models\Giraffe;
use App\Models\Monkey;
use Predis\Client;

class RedisSerializer
{
    public function __construct(private Client $redis)
    {
    }

    public function saveAnimal(Animal $animal): void
    {
        $previous = $this->redis->get($animal->getName());
        $this->redis->set($animal->getName(), $animal->getHealth());
        if ($previous) {
            $this->redis->set($animal->getName() . '_previous', $animal->getHealth());
        }
    }

    public function getAnimal(string $name): Animal
    {
        $record = $this->redis->get($name);
        $previous = $this->redis->get($name . '_previous');
        $tag = explode('_', $name);

        $animal = match ($tag[0]) {
            'elephant' => new Elephant($tag[1], $previous),
            'giraffe' => new Giraffe($tag[1]),
            'monkey' => new Monkey($tag[1])
        };
        $animal->setHealth((float)$record);
        return $animal;
    }

    public function getZooTime(): int
    {
        return $this->redis->get('zooTime') ? (int)$this->redis->get('zooTime') : 0;
    }

    public function advanceZooTime(): void
    {
        $this->redis->set('zooTime', $this->getZooTime() + 1);
    }

    public function restartZoo(): void
    {
        $this->redis->flushall();
    }

}