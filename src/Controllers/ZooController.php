<?php

namespace App\Controllers;

use App\Core\BaseController;
use App\Models\Animal;
use App\Models\Elephant;
use App\Models\Giraffe;
use App\Models\Monkey;
use Exception;

class ZooController extends BaseController
{
    public const INITIAL_ANIMAL_AMOUNT = 5;

    public function index($param): void
    {
        $time = $this->get('RedisSerializer')->getZooTime();
        if ($time === 0) {
            $this->initializeZoo();
        }
        $this->render('Zoo/index.html.twig', [
            'time' => $time,
            'zoo' =>$this->getAnimals()
        ]);
    }

    public function feedAnimals()
    {
        $zooAnimals = $this->getAnimals();
        foreach ($zooAnimals as $type) {
            $affectedHealth = random_int(10, 25);
            /** @var Animal $animal */
            foreach ($type as $animal) {
                if ($animal->isAlive()) {
                    $newHealth = $animal->getHealth() + ($animal->getHealth() * $affectedHealth) / 100;
                    $animal->setHealth(min($newHealth, 100));
                    $animal->calculateNewHealth();
                    $this->get('RedisSerializer')->saveAnimal($animal);
                }
            }
        }
        $this->redirect('/');
    }

    /**
     * @throws Exception
     */
    public function addHour()
    {
        $zooAnimals = $this->getAnimals();
        foreach ($zooAnimals as $type) {
            /** @var Animal $animal */
            foreach ($type as $animal) {
                $affectedHealth = random_int(0, 20);
                $newHealth = $animal->getHealth() - ($animal->getHealth() * $affectedHealth) / 100;
                $animal->setHealth(max($newHealth, 0));
                $animal->calculateNewHealth();
                $this->get('RedisSerializer')->saveAnimal($animal);
            }
        }
        $this->get('RedisSerializer')->advanceZooTime();
        $this->redirect('/');
    }

    public function restartZoo()
    {
        $this->get('RedisSerializer')->restartZoo();
        $this->redirect('/');
    }

    private function initializeZoo(): void
    {
        for ($i = 1; $i <= self::INITIAL_ANIMAL_AMOUNT; $i++) {
            $elephant = new Elephant($i, null);
            $this->get('RedisSerializer')->saveAnimal($elephant);
        }
        for ($i = 1; $i <= self::INITIAL_ANIMAL_AMOUNT; $i++) {
            $giraffe = new Giraffe($i);
            $this->get('RedisSerializer')->saveAnimal($giraffe);
        }
        for ($i = 1; $i <= self::INITIAL_ANIMAL_AMOUNT; $i++) {
            $monkey = new Monkey($i);
            $this->get('RedisSerializer')->saveAnimal($monkey);
        }
    }

    private function getAnimals(): array
    {
        $elephants = $giraffes = $monkeys = [];
        for ($i = 1; $i <= self::INITIAL_ANIMAL_AMOUNT; $i++) {
            $elephants[] = $this->get('RedisSerializer')->getAnimal('elephant_' . $i);
        }
        for ($i = 1; $i <= self::INITIAL_ANIMAL_AMOUNT; $i++) {
            $giraffes[] = $this->get('RedisSerializer')->getAnimal('giraffe_' . $i);
        }
        for ($i = 1; $i <= self::INITIAL_ANIMAL_AMOUNT; $i++) {
            $monkeys[] = $this->get('RedisSerializer')->getAnimal('monkey_' . $i);
        }


        return [
            'Elephants' => $elephants,
            'Giraffes' => $giraffes,
            'Monkeys' => $monkeys
        ];
    }

}