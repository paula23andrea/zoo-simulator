<?php

namespace App\Models;

abstract class Animal
{

    public function __construct(protected int $tag, protected float $health = 100)
    {
    }

    abstract protected function calculateNewHealth(): void;
    abstract public function getName(): string;
    abstract public function getIcon(): string;

    /**
     * @return bool
     */
    public function isAlive(): bool
    {
        return $this->health > 0;
    }

    /**
     * @return float|int
     */
    public function getHealth(): float|int
    {
        return $this->health;
    }

    /**
     * @param float|int $health
     */
    public function setHealth(float|int $health): void
    {
        $this->health = $health;
    }
}